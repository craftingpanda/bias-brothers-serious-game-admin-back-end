package com.biasbrothers.seriousgame.round;

import com.biasbrothers.seriousgame.canvas.Canvas;
import com.biasbrothers.seriousgame.scenario.Scenario;
import com.biasbrothers.seriousgame.timer.Timer;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Round")
@Table(name = "round")
public class Round {

    @Id
    @SequenceGenerator(
            name = "round_sequence",
            sequenceName = "round_sequence",
            initialValue = 7,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "round_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "title",
            nullable = false)
    private String title;

    @Column(
            name = "round_number",
            nullable = false)
    private int roundNumber;

    @ManyToMany
    @JoinTable(name = "canvas_round",
            joinColumns = @JoinColumn(name = "canvas_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "round_id", referencedColumnName = "id"))
    private List<Canvas> canvases;

    @ManyToOne
    @JoinColumn(name = "scenario_id")
    private Scenario scenario;

    @ManyToOne
    @JoinColumn(name = "timer_id")
    private Timer timer;

    public Round() {

    }

    public Round(String title, int roundNumber, List<Canvas> canvases, Scenario scenario, Timer timer) {
        this.title = title;
        this.roundNumber = roundNumber;
        this.canvases = canvases;
        this.scenario = scenario;
        this.timer = timer;
    }

    public Round(Long id, String title, int roundNumber, List<Canvas> canvases, Scenario scenario, Timer timer) {
        this(title, roundNumber, canvases, scenario, timer);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Canvas> getCanvases() {
        return canvases;
    }

    public void setCanvases(List<Canvas> canvases) {
        this.canvases = canvases;
    }

    public Scenario getScenario() {
        return scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public Timer getTimer() {
        return timer;
    }

    public void setTimer(Timer timer) {
        this.timer = timer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }

    public void add(Canvas canvas) {
        canvases.add(canvas);
    }

    public void delete(Canvas canvas) {
        canvases.remove(canvas);
    }

    @Override
    public Round clone() {
        try {
            return (Round) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Round(this.getId(), this.getTitle(), this.getRoundNumber(), this.getCanvases(), this.getScenario(), this.getTimer());
        }
    }

    @Override
    public String toString() {
        return "Round{" +
                "id=" + id +
                ", canvases=" + canvases +
                ", scenario=" + scenario +
                ", timer=" + timer +
                '}';
    }
}
