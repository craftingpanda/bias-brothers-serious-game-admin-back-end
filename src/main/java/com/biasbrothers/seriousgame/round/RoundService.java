package com.biasbrothers.seriousgame.round;

import com.biasbrothers.seriousgame.biasQuestion.BiasQuestionRepository;
import com.biasbrothers.seriousgame.canvas.CanvasRepository;
import com.biasbrothers.seriousgame.scenario.ScenarioRepository;
import com.biasbrothers.seriousgame.timer.TimerRepository;
import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoundService extends ImplementService<RoundRepository, Round, Long> {
    private final RoundRepository roundRepository;
    private final CanvasRepository canvasRepository;
    private final ScenarioRepository scenarioRepository;
    private final TimerRepository timerRepository;

    @Autowired
    public RoundService(RoundRepository roundRepository, CanvasRepository canvasRepository, ScenarioRepository scenarioRepository, TimerRepository timerRepository) {
        super(roundRepository);
        this.roundRepository = roundRepository;
        this.canvasRepository = canvasRepository;
        this.scenarioRepository = scenarioRepository;
        this.timerRepository = timerRepository;
    }

    public Round add(Round round) {
        return save(round);
    }

    public Round update(Round round) {
        try {
            if (roundRepository.existsById(round.getId())) {
                return save(round);
            } else {
                throw new IllegalStateException("round with id " + round.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }

    private Round save(Round round) {
        timerRepository.save(round.getTimer());
        canvasRepository.saveAll(round.getCanvases());
        scenarioRepository.save(round.getScenario().clone());

        return roundRepository.save(round);
    }
}
