package com.biasbrothers.seriousgame.canvas;

import com.biasbrothers.seriousgame.canvas.metric.MetricRepository;
import com.biasbrothers.seriousgame.newsarticle.NewsArticleRepository;
import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CanvasService extends ImplementService<CanvasRepository, Canvas, Long> {
    private final CanvasRepository canvasRepository;
    private final MetricRepository metricRepository;
    private final NewsArticleRepository newsArticleRepository;

    @Autowired
    public CanvasService(CanvasRepository canvasRepository, MetricRepository metricRepository, NewsArticleRepository newsArticleRepository) {
        super(canvasRepository);
        this.canvasRepository = canvasRepository;
        this.metricRepository = metricRepository;
        this.newsArticleRepository = newsArticleRepository;
    }

    public Canvas add(Canvas canvas) {
        return save(canvas);
    }

    public Canvas update(Canvas canvas) {
        try {
            if (canvasRepository.existsById(canvas.getId())) {
                return save(canvas);
            } else {
                throw new IllegalStateException("canvas with id " + canvas.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }

    private Canvas save(Canvas canvas) {
        metricRepository.saveAll(canvas.getMetrics());
        newsArticleRepository.saveAll(canvas.getNewsArticles());

        return canvasRepository.save(canvas.clone());
    }

}
