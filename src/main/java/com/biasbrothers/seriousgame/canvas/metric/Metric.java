package com.biasbrothers.seriousgame.canvas.metric;

import javax.persistence.*;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Metric")
@Table(
        name = "metric",
        uniqueConstraints = {@UniqueConstraint(name = "metric_name_unique", columnNames = "name")}
)
public class Metric {

    @Id
    @SequenceGenerator(
            name = "metric_sequence",
            sequenceName = "metric_sequence",
            initialValue = 4,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "metric_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "name",
            nullable = false)
    private String name;

    public Metric() {

    }

    public Metric(String name) {
        this.name = name;
    }

    public Metric(Long id, String name) {
        this(name);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Metric clone() {
        try {
            return (Metric) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Metric(this.getId(), this.getName());
        }
    }
}
