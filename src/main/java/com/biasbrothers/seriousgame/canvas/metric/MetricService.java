package com.biasbrothers.seriousgame.canvas.metric;

import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MetricService extends ImplementService<MetricRepository, Metric, Long> {
    private final MetricRepository metricRepository;

    @Autowired
    public MetricService(MetricRepository metricRepository) {
        super(metricRepository);
        this.metricRepository = metricRepository;
    }

    public Metric add(Metric metric) {
        return metricRepository.save(metric.clone());
    }

    @Transactional
    public Metric update(Metric metric) {
        try {
            if(metricRepository.existsById(metric.getId())) {
                return metricRepository.save(metric.clone());
            } else {
                throw new IllegalStateException("metric with id " + metric.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }
}
