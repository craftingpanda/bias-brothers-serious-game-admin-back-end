package com.biasbrothers.seriousgame.canvas.metric;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@EnableJpaRepositories
public interface MetricRepository extends JpaRepository<Metric, Long> {
}
