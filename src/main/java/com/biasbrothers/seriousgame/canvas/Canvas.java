package com.biasbrothers.seriousgame.canvas;

import com.biasbrothers.seriousgame.canvas.metric.Metric;
import com.biasbrothers.seriousgame.newsarticle.NewsArticle;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Canvas")
@Table(
        name = "canvas"
)
public class Canvas {

    @Id
    @SequenceGenerator(
            name = "canvas_sequence",
            sequenceName = "canvas_sequence",
            initialValue = 26,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "canvas_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "name",
            nullable = false)
    private String name;

    @Column(name = "points")
    private int points;

    @Column(name = "value1")
    private String value1;

    @Column(name = "value2")
    private String value2;

    @Column(name = "value3")
    private String value3;

    @ManyToMany
    @JoinTable(name = "metric_canvas",
            joinColumns = @JoinColumn(name = "canvas_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "metric_id", referencedColumnName = "id"))
    private List<Metric> metrics;

    @ManyToMany
    @JoinTable(name = "news_article_canvas",
            joinColumns = @JoinColumn(name = "news_article_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "canvas_id", referencedColumnName = "id"))
    private List<NewsArticle> newsArticles;

    public Canvas() {
    }

    public Canvas(String name, int points, String value1, String value2, String value3, List<Metric> metrics, List<NewsArticle> newsArticles) {
        this.name = name;
        this.points = points;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.metrics = metrics;
        this.newsArticles = newsArticles;
    }

    public Canvas(Long id, String name, int points, String value1, String value2, String value3, List<Metric> metrics, List<NewsArticle> newsArticles) {
        this(name, points, value1, value2, value3, metrics, newsArticles);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getValue3() {
        return value3;
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Metric> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<Metric> metrics) {
        this.metrics = metrics;
    }

    public void add(Metric metric, NewsArticle newsArticle) {
        metrics.add(metric);
        newsArticles.add(newsArticle);
    }

    public void delete(Metric metric, NewsArticle newsArticle) {
        metrics.remove(metric);
        newsArticles.remove(newsArticle);
    }

    public List<NewsArticle> getNewsArticles() {
        return newsArticles;
    }

    public void setNewsArticles(List<NewsArticle> newsArticles) {
        this.newsArticles = newsArticles;
    }

    @Override
    public Canvas clone() {
        try {
            return (Canvas) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Canvas(this.getId(), this.getName(), this.getPoints(), this.getValue1(), this.getValue2(), this.getValue3(), this.getMetrics(), this.getNewsArticles());
        }
    }

    @Override
    public String toString() {
        return "Canvas{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", points=" + points + '\'' +
                ", value1='" + value1 + '\'' +
                ", value2='" + value2 + '\'' +
                ", value3='" + value3 + '\'' +
                ", metrics=" + metrics +
                ", newsArticles=" + newsArticles +
                '}';
    }
}
