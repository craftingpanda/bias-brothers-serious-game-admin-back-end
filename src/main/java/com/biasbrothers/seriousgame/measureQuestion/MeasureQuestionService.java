package com.biasbrothers.seriousgame.measureQuestion;

import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MeasureQuestionService extends ImplementService<MeasureQuestionRepository, MeasureQuestion, Long> {
    private final MeasureQuestionRepository measureQuestionRepository;

    @Autowired
    public MeasureQuestionService(MeasureQuestionRepository measureQuestionRepository) {
        super(measureQuestionRepository);
        this.measureQuestionRepository = measureQuestionRepository;
    }

    public MeasureQuestion add(MeasureQuestion measureQuestion) {
        return measureQuestionRepository.save(measureQuestion.clone());
    }

    @Transactional
    public MeasureQuestion update(MeasureQuestion measureQuestion) {
        try {
            if (measureQuestionRepository.existsById(measureQuestion.getId())) {
                return measureQuestionRepository.save(measureQuestion.clone());
            } else {
                throw new IllegalStateException("measure question with id " + measureQuestion.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }
}
