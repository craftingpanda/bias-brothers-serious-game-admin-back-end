package com.biasbrothers.seriousgame.measureQuestion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/measure_question")
public class MeasureQuestionController {
    private final MeasureQuestionService measureQuestionService;

    @Autowired
    public MeasureQuestionController(MeasureQuestionService measureQuestionService) {
        this.measureQuestionService = measureQuestionService;
    }

    @CrossOrigin
    @GetMapping
    public List<MeasureQuestion> getAll() {
        return measureQuestionService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public MeasureQuestion getById(@RequestParam Long id) {
        return measureQuestionService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<MeasureQuestion> add(@RequestBody MeasureQuestion measureQuestion) {
        MeasureQuestion measureQuestionTest = measureQuestionService.add(measureQuestion);

        return new ResponseEntity<>(measureQuestionTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        measureQuestionService.delete(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<MeasureQuestion> update(@RequestBody MeasureQuestion measureQuestion ) {
        MeasureQuestion measureQuestionTest = measureQuestionService.update(measureQuestion);

        return new ResponseEntity<>(measureQuestionTest, HttpStatus.ACCEPTED);
    }
}
