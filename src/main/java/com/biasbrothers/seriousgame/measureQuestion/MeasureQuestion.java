package com.biasbrothers.seriousgame.measureQuestion;

import com.biasbrothers.seriousgame.measure.Measure;

import javax.persistence.*;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "MeasureQuestion")
@Table(
        name = "measure_question"
)
public class MeasureQuestion {

    @Id
    @SequenceGenerator(
            name = "measure_question_sequence",
            sequenceName = "measure_question_sequence",
            initialValue = 19,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "measure_question_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "points",
            nullable = false
    )
    private int points;

    @ManyToOne
    @JoinColumn(name = "measure_id", foreignKey = @ForeignKey(name = "measure_id_uniqueKey"))
    private Measure measure;

    public MeasureQuestion() {

    }

    public MeasureQuestion(int points, Measure measure) {
        this.points = points;
        this.measure = measure;
    }

    public MeasureQuestion(Long id, int points, Measure measure) {
        this(points, measure);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Measure getMeasure() {
        return measure;
    }

    public void setMeasure(Measure measure) {
        this.measure = measure;
    }

    @Override
    public MeasureQuestion clone() {
        try {
            return (MeasureQuestion) super.clone();
        } catch (CloneNotSupportedException e) {
            return new MeasureQuestion(this.getId(), this.getPoints(), this.getMeasure());
        }
    }
}
