package com.biasbrothers.seriousgame.bias;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Bias")
@Table(
        name = "bias",
        uniqueConstraints = {@UniqueConstraint(name = "bias_name_unique", columnNames = "name")}
)
public class Bias {

    @Id
    @SequenceGenerator(
            name = "bias_sequence",
            sequenceName = "bias_sequence",
            initialValue = 19,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "bias_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "name",
            nullable = false)
    private String name;

    @Column(
            name = "description",
            nullable = false,
            columnDefinition = "TEXT")
    private String description;

    @Column(
            name = "example",
            nullable = false,
            columnDefinition = "TEXT")
    private String example;

    public Bias() {

    }

    public Bias(String name, String description, String example) {
        this.name = name;
        this.description = description;
        this.example = example;
    }

    public Bias(Long id, String name, String description, String example) {
        this(name, description, example);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    @Override
    public Bias clone() {
        try {
            return (Bias)super.clone();
        } catch (CloneNotSupportedException e) {
            return new Bias(this.getId(), this.getName(), this.getDescription(), this.getExample());
        }
    }
}