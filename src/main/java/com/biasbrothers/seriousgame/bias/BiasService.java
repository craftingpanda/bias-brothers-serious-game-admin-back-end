package com.biasbrothers.seriousgame.bias;

import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BiasService extends ImplementService<BiasRepository, Bias, Long> {
    private final BiasRepository biasRepository;

    @Autowired
    public BiasService(BiasRepository biasRepository) {
        super(biasRepository);
        this.biasRepository = biasRepository;
    }

    public Bias add(Bias bias) {
        return biasRepository.save(bias.clone());
    }

    @Transactional
    public Bias update(Bias bias) {
        try {
            if (biasRepository.existsById(bias.getId())) {
                return biasRepository.save(bias.clone());
            } else {
                throw new IllegalStateException("bias with id " + bias.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }
}
