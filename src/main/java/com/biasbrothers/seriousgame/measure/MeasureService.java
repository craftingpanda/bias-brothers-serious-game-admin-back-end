package com.biasbrothers.seriousgame.measure;

import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MeasureService extends ImplementService<MeasureRepository, Measure, Long> {
    private final MeasureRepository measureRepository;

    @Autowired
    public MeasureService(MeasureRepository measureRepository) {
        super(measureRepository);
        this.measureRepository = measureRepository;
    }

    public Measure add(Measure measure) {
        return measureRepository.save(measure.clone());
    }

    @Transactional
    public Measure update(Measure measure) {
        try {
            if (measureRepository.existsById(measure.getId())){
                return measureRepository.save(measure.clone());
            } else {
                throw new IllegalStateException("measure with id " + measure.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }
}
