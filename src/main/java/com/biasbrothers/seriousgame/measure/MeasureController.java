package com.biasbrothers.seriousgame.measure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/measure")
public class MeasureController {
    private final MeasureService measureService;

    @Autowired
    public MeasureController(MeasureService measureService) {
        this.measureService = measureService;
    }

    @CrossOrigin
    @GetMapping
    public List<Measure> getAll() {
        return measureService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public Measure getById(@RequestParam Long id) {
        return measureService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<Measure> add(@RequestBody Measure measure) {
        Measure measureTest = measureService.add(measure);

        return new ResponseEntity<>(measureTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        measureService.delete(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<Measure> update(@RequestBody Measure measure ) {
        Measure measureTest = measureService.update(measure);

        return new ResponseEntity<>(measureTest, HttpStatus.ACCEPTED);
    }
}
