package com.biasbrothers.seriousgame.measure;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Measure")
@Table(
        name = "measure",
        uniqueConstraints = {@UniqueConstraint(name = "measure_unique", columnNames = "answer")}
)
public class Measure {

    @Id
    @SequenceGenerator(
            name = "measure_sequence",
            sequenceName = "measure_sequence",
            initialValue = 19,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "measure_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "answer",
            nullable = false)
    private String answer;

    public Measure() {

    }

    public Measure(String answer) {
        this.answer = answer;
    }

    public Measure(Long id, String answer) {
        this(answer);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public Measure clone() {
        try {
            return (Measure) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Measure(this.getId(), this.getAnswer());
        }
    }
}
