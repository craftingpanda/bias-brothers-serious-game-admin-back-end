package com.biasbrothers.seriousgame.biasQuestion;

import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BiasQuestionService extends ImplementService<BiasQuestionRepository, BiasQuestion, Long> {
    private final BiasQuestionRepository biasQuestionRepository;

    @Autowired
    public BiasQuestionService(BiasQuestionRepository biasQuestionRepository) {
        super(biasQuestionRepository);
        this.biasQuestionRepository = biasQuestionRepository;
    }

    public BiasQuestion add(BiasQuestion biasQuestion) {
        return biasQuestionRepository.save(biasQuestion.clone());
    }

    @Transactional
    public BiasQuestion update(BiasQuestion biasQuestion) {
        try {
            if (biasQuestionRepository.existsById(biasQuestion.getId())) {
                return biasQuestionRepository.save(biasQuestion.clone());
            } else {
                throw new IllegalStateException("biasQuestion with id " + biasQuestion.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }

}
