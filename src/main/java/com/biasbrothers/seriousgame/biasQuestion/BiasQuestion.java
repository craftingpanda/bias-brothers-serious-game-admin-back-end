package com.biasbrothers.seriousgame.biasQuestion;

import com.biasbrothers.seriousgame.bias.Bias;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "BiasQuestion")
@Table(
        name = "bias_question")
public class BiasQuestion {

    @Id
    @SequenceGenerator(
            name = "bias_question_sequence",
            sequenceName = "bias_question_sequence",
            initialValue = 19,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "bias_question_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "points",
            nullable = false)
    private int points;

    @ManyToOne
    @JoinColumn(name = "bias_id", foreignKey = @ForeignKey(name = "bias_id_uniqueKey"))
    private Bias bias;

    public BiasQuestion() {
    }

    public BiasQuestion(int points, Bias bias) {
        this.points = points;
        this.bias = bias;
    }

    public BiasQuestion(Long id, int points, Bias bias) {
        this(points, bias);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Bias getBias() {
        return bias;
    }

    public void setBias(Bias bias) {
        this.bias = bias;
    }

    @Override
    public BiasQuestion clone() {
        try {
            return (BiasQuestion) super.clone();
        } catch (CloneNotSupportedException e) {
            return new BiasQuestion(this.getId(), this.getPoints(), this.getBias());
        }
    }

    @Override
    public String toString() {
        return "BiasQuestion{" +
                "id=" + id +
                ", points=" + points +
                ", bias=" + bias +
                '}';
    }
}
