package com.biasbrothers.seriousgame.timer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/timer")
public class TimerController {
    private final TimerService timerService;

    @Autowired
    public TimerController(TimerService timerService) {
        this.timerService = timerService;
    }

    @CrossOrigin
    @GetMapping
    public List<Timer> getAll() {
        return timerService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public Timer getById(@RequestParam Long id) {
        return timerService.getById(id);
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<Timer> add(@RequestBody Timer timer) {
        Timer timerTest = timerService.add(timer);

        return new ResponseEntity<>(timerTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {

        timerService.delete(id);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<Timer> update(@RequestBody Timer timer) {
        Timer timerTest = timerService.update(timer);

        return new ResponseEntity<>(timerTest, HttpStatus.ACCEPTED);
    }
}
