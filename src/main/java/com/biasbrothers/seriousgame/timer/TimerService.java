package com.biasbrothers.seriousgame.timer;

import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TimerService extends ImplementService<TimerRepository, Timer, Long> {
    private final TimerRepository timerRepository;

    @Autowired
    public TimerService(TimerRepository timerRepository) {
        super(timerRepository);
        this.timerRepository = timerRepository;
    }

    public Timer add(Timer timer) {
        return timerRepository.save(timer.clone());
    }

    @Transactional
    public Timer update(Timer timer) {
        try {
            if (timerRepository.existsById(timer.getId())) {
                return timerRepository.save(timer.clone());
            } else {
                throw new IllegalStateException("timer with id " + timer.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }
}