package com.biasbrothers.seriousgame.timer;

import com.biasbrothers.seriousgame.round.Round;

import javax.persistence.*;

import java.util.Collection;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Timer")
@Table(
        name = "timer",
        uniqueConstraints = {@UniqueConstraint(name = "name_unique", columnNames = "name")}
)
public class Timer {

    @Id
    @SequenceGenerator(
            name = "timer_sequence",
            sequenceName = "timer_sequence",
            initialValue = 5,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "timer_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "name",
            nullable = false)
    private String name;

    public Timer() {
    }

    public Timer(String name) {
        this.name = name;
    }

    public Timer(Long id, String name) {
        this(name);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Timer clone() {
        try {
            return (Timer) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Timer(this.getId(), this.getName());
        }
    }
}
