package com.biasbrothers.seriousgame.game;

import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameService extends ImplementService<GameRepository, Game, Long> {
    private final GameRepository gameRepository;

    @Autowired
    public GameService(GameRepository gameRepository) {
        super(gameRepository);
        this.gameRepository = gameRepository;
    }

    public Game add(Game game) {
        return gameRepository.save(game.clone());
    }

    public Game update(Game game) {
        try {
            if (gameRepository.existsById(game.getId())) {
                return gameRepository.save(game.clone());
            } else {
                throw new IllegalStateException("game with id " + game.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }

}
