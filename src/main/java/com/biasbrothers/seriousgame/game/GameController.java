package com.biasbrothers.seriousgame.game;

import com.biasbrothers.seriousgame.round.Round;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/game")
public class GameController {
    private final GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @CrossOrigin
    @GetMapping
    public List<Game> getAll() {
        return gameService.getAll();
    }

    @CrossOrigin
    @GetMapping(path = "/")
    public Game getById(@RequestParam Long id) {
        return gameService.getById(id);
    }

    @CrossOrigin
    @GetMapping(path = "{id}/round/{roundNumber}")
    public Round getRound(@PathVariable("id") int idGame,
                          @PathVariable("roundNumber") int roundNumber) {
        List<Round> rounds = gameService.getById((long) idGame).getRounds();

        Round getRound = new Round();

        for (Round round: rounds) {
            if (Objects.equals(round.getRoundNumber(), roundNumber)) {
                getRound = round.clone();
            }
        }
        return getRound;
    }

    @CrossOrigin
    @PostMapping(path = "/add")
    public ResponseEntity<Game> add(@RequestBody Game game) {
        Game gameTest = gameService.add(game);

        return new ResponseEntity<>(gameTest, HttpStatus.CREATED);
    }

    @CrossOrigin
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        gameService.delete(id);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @CrossOrigin
    @PutMapping(path = "/update/")
    public ResponseEntity<Game> update(@RequestBody Game game) {
        Game gameTest = gameService.update(game);

        return new ResponseEntity<>(gameTest, HttpStatus.ACCEPTED);
    }
}
