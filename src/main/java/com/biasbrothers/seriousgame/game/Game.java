package com.biasbrothers.seriousgame.game;

import com.biasbrothers.seriousgame.round.Round;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Game")
@Table(name = "game")
public class Game {
    @Id
    @SequenceGenerator(
            name = "game_sequence",
            sequenceName = "game_sequence",
            initialValue = 2,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "game_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "name",
            nullable = false)
    private String name;

    @ManyToMany
    @JoinTable(name = "round_game",
            joinColumns = @JoinColumn(name = "game_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "round_id", referencedColumnName = "id"))
    @OrderBy("roundNumber ASC")
    private List<Round> rounds;

    public Game() {
    }

    public Game(String name, List<Round> rounds) {
        this.name = name;
        this.rounds = rounds;
    }

    public Game(Long id, String name, List<Round> rounds) {
        this(name, rounds);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void add(Round round) {
        rounds.add(round);
    }

    public void delete(Round round) {
        rounds.remove(round);
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    @Override
    public Game clone() {
        try {
            return (Game) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Game(this.getId(), this.getName(), this.getRounds());
        }
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", rounds=" + rounds +
                '}';
    }

}
