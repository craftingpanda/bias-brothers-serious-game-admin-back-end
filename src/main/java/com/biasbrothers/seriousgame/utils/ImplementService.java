package com.biasbrothers.seriousgame.utils;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public class ImplementService<R extends JpaRepository<T, K>, T, K> {
    private final R repository;

    public ImplementService(R repository) {
        this.repository = repository;
    }

    public List<T> getAll() {
        return repository.findAll();
    }

    public T getById(K id) {
        return repository.findById(id).orElseThrow(() -> new IllegalStateException("item with id " + id + " does not exist"));
    }

    public String delete(K id) {
        repository.deleteById(id);
        return "SUCCESS";
    }

    public T add(T item) {
        throw new UnsupportedOperationException("not implemented");
    }

    public T update(T item) {
        throw new UnsupportedOperationException("not implemented");
    }
}
