package com.biasbrothers.seriousgame.utils.jsonexporter;

import com.biasbrothers.seriousgame.bias.Bias;
import com.biasbrothers.seriousgame.round.Round;

import java.util.List;

public interface JsonExporter {
    String exportRoundJson(Round round);
    String exportBiasJson(List<Bias> biasList);
}