package com.biasbrothers.seriousgame.utils.jsonexporter.rounddownloader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("download")
public class DownloadController {

    private final ExporterService exporterService;

    @Autowired
    public DownloadController(ExporterService exporterService) {
        this.exporterService = exporterService;
    }

    @GetMapping("/round/{id}")
    public ResponseEntity<byte[]> downloadRound(@PathVariable Long id) {
        String roundJsonString = exporterService.roundJson(id);
        int newRoundNumber = exporterService.getRoundNumber(id);

        byte[] roundJsonBytes = roundJsonString.getBytes();

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=round" + newRoundNumber + ".json")
                .contentType(MediaType.APPLICATION_JSON)
                .contentLength(roundJsonBytes.length)
                .body(roundJsonBytes);

    }

    @GetMapping("/bias_list")
    public ResponseEntity<byte[]> downloadBiasList() {
        String biasJsonString = exporterService.biasJson();
        byte[] biasJsonBytes = biasJsonString.getBytes();

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=BiasList.json")
                .contentType(MediaType.APPLICATION_JSON)
                .contentLength(biasJsonBytes.length)
                .body(biasJsonBytes);

    }
}

