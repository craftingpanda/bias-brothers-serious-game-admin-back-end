package com.biasbrothers.seriousgame.utils.jsonexporter.rounddownloader;

import com.biasbrothers.seriousgame.bias.Bias;
import com.biasbrothers.seriousgame.round.Round;
import com.biasbrothers.seriousgame.utils.jsonexporter.JsonExporter;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JsonExporterImpl implements JsonExporter {
    @Override
    public String exportRoundJson(Round round) {
        Gson gson = new Gson();
        return gson.toJson(round);
    }

    @Override
    public String exportBiasJson(List<Bias> biasList) {
        Gson gson = new Gson();
        return gson.toJson(biasList);
    }
}
