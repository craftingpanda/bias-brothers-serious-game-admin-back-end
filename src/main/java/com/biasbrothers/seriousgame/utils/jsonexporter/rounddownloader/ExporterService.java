package com.biasbrothers.seriousgame.utils.jsonexporter.rounddownloader;

import com.biasbrothers.seriousgame.bias.Bias;
import com.biasbrothers.seriousgame.bias.BiasService;
import com.biasbrothers.seriousgame.round.Round;
import com.biasbrothers.seriousgame.round.RoundService;
import com.biasbrothers.seriousgame.utils.jsonexporter.JsonExporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExporterService {

    private final BiasService biasService;
    private final RoundService roundService;
    private final JsonExporter jsonExporter;

    @Autowired
    public ExporterService(BiasService biasService, RoundService roundService, JsonExporter jsonExporter) {
        this.biasService = biasService;
        this.roundService = roundService;
        this.jsonExporter = jsonExporter;
    }

    public String biasJson() {
        List<Bias> biasList = biasService.getAll();

        return jsonExporter.exportBiasJson(biasList);
    }

    public String roundJson(Long id) {
        Round fetchedRound = roundService.getById(id);

        if (id >= 1 && id <= roundService.getAll().size()) {
            return jsonExporter.exportRoundJson(fetchedRound);
        } else {
            throw new IllegalStateException("Round number "  + id + " does not exist");
        }
    }

    public int getRoundNumber(Long id) {
        Round fetchedRound = roundService.getById(id);

        if (id >= 1 && id <= roundService.getAll().size()) {
            return fetchedRound.getRoundNumber();
        } else {
            throw new IllegalStateException("Round number "  + id + " does not exist");
        }
    }
}
