package com.biasbrothers.seriousgame.gameLogic;

import com.biasbrothers.seriousgame.game.Game;
import com.biasbrothers.seriousgame.game.GameService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/game_logic")
public class GameLogicController {
    private final GameLogicHandler gameLogicHandler = new GameLogicHandler();
    private final GameService gameService;

    public GameLogicController(GameService gameService) {
        this.gameService = gameService;
    }

    @CrossOrigin
    @GetMapping(path = "/{canvasNumber}/{measurePoints}")
    public String checkAnswer(@PathVariable("canvasNumber") int canvasNumber,
                              @PathVariable("measurePoints") int measurePoints) throws IllegalStateException {
        try {
            if (canvasNumber <= 4 && canvasNumber >= 0) {
                if (measurePoints == 0 || measurePoints == 2 || measurePoints == 5) {
                    return String.valueOf(gameLogicHandler.checkAnswer(canvasNumber, measurePoints));
                }
                else {
                    throw new IllegalStateException("Error: measure points not correct");
                }
            } else {
                throw new IllegalStateException("Error: canvas number not correct");
            }
        } catch (IllegalStateException e) {
            return e.getMessage();
        }
    }

    @CrossOrigin
    @GetMapping(path = "/game/")
    public int checkRounds(@RequestParam Long id) {
        Game game = gameService.getById(id).clone();

        return gameLogicHandler.checkRounds(game);
    }

}
