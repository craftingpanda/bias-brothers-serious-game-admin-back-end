package com.biasbrothers.seriousgame.gameLogic;

import com.biasbrothers.seriousgame.game.Game;

public class GameLogicHandler {

    //returns canvasNumber
    public int checkAnswer(int canvasNumber, int measurePoints) {
        if (measurePoints == 0) {
            switch (canvasNumber) {
                case 0:
                case 1:
                    return 0;
                case 2:
                    return 1;
                case 3:
                    return 2;
                case 4:
                    return 3;
            }
        } else if (measurePoints == 2) {
            switch (canvasNumber) {
                case 0:
                    return 0;
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                case 4:
                    return 3;
            }
        } else if (measurePoints == 5) {
            switch (canvasNumber) {
                case 0:
                    return 1;
                case 1:
                    return 2;
                case 2:
                    return 3;
                case 3:
                case 4:
                    return 4;
            }
        }
        return canvasNumber;
    }

    //returns total rounds of the game
    public int checkRounds(Game game) {
        Game newGame = game.clone();

        return newGame.getRounds().size() + 1;
    }
}

