package com.biasbrothers.seriousgame.newsarticle;

import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NewsArticleService extends ImplementService<NewsArticleRepository, NewsArticle, Long> {
    private final NewsArticleRepository newsArticleRepository;

    @Autowired
    public NewsArticleService(NewsArticleRepository newsArticleRepository) {
        super(newsArticleRepository);
        this.newsArticleRepository = newsArticleRepository;
    }

    public NewsArticle add(NewsArticle newsArticle) {
        return newsArticleRepository.save(newsArticle.clone());
    }

    @Transactional
    public NewsArticle update(NewsArticle newsArticle) {
        try {
            if (newsArticleRepository.existsById(newsArticle.getId())) {
                return newsArticleRepository.save(newsArticle.clone());
            } else {
                throw new IllegalStateException("news article with id " + newsArticle.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }
}
