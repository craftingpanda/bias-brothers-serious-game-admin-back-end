package com.biasbrothers.seriousgame.newsarticle;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@EnableJpaRepositories
public interface NewsArticleRepository extends JpaRepository<NewsArticle, Long> {
}
