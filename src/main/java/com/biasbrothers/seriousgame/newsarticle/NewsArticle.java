package com.biasbrothers.seriousgame.newsarticle;

import javax.persistence.*;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "NewsArticle")
@Table(name = "news_article")
public class NewsArticle {

    @Id
    @SequenceGenerator(
            name = "news_article_sequence",
            sequenceName = "news_article_sequence",
            initialValue = 34,
            allocationSize = 1)
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "news_article_sequence")
    @Column(
            name = "id",
            updatable = false)
    private Long id;

    @Column(
            name = "title",
            nullable = false)
    private String title;

    @Column(
            name = "message",
            nullable = false,
            columnDefinition = "TEXT")
    private String message;

    @Column(
            name = "source",
            nullable = false)
    private String source;

    @Column(
            name = "pop_up",
            columnDefinition = "boolean default false")
    private Boolean popUp = false;

    public NewsArticle() {
    }

    public NewsArticle(String title, String message, String source, Boolean popUp) {
        this.title = title;
        this.message = message;
        this.source = source;
        this.popUp = popUp;
    }

    public NewsArticle(Long id, String title, String message, String source, Boolean popUp) {
        this(title, message, source, popUp);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Boolean getPopUp() {
        return popUp;
    }

    public void setPopUp(Boolean popUp) {
        this.popUp = popUp;
    }

    @Override
    public NewsArticle clone() {
        try {
            return (NewsArticle) super.clone();
        } catch (CloneNotSupportedException e) {
            return new NewsArticle(this.getId(), this.getTitle(), this.getMessage(), this.getSource(), this.getPopUp());
        }
    }
}
