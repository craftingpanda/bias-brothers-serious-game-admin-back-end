package com.biasbrothers.seriousgame.scenario;

import com.biasbrothers.seriousgame.biasQuestion.BiasQuestionRepository;
import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestionRepository;
import com.biasbrothers.seriousgame.utils.ImplementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class ScenarioService extends ImplementService<ScenarioRepository, Scenario, Long> {
    private final ScenarioRepository scenarioRepository;
    private final MeasureQuestionRepository measureQuestionRepository;
    private final BiasQuestionRepository biasQuestionRepository;

    @Autowired
    public ScenarioService(ScenarioRepository scenarioRepository, MeasureQuestionRepository measureQuestionRepository, BiasQuestionRepository biasQuestionRepository) {
        super(scenarioRepository);
        this.scenarioRepository = scenarioRepository;
        this.measureQuestionRepository = measureQuestionRepository;
        this.biasQuestionRepository = biasQuestionRepository;
    }

    public Scenario add(Scenario scenario) {
        return save(scenario);
    }

    @Transactional
    public Scenario update(Scenario scenario) {
        try {
            if (scenarioRepository.existsById(scenario.getId())) {
                return save(scenario);
            } else {
                throw new IllegalStateException("timer with id " + scenario.getId() + " does not exist");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }

    private Scenario save(Scenario scenario) {
        measureQuestionRepository.saveAll(scenario.getMeasureQuestions());
        biasQuestionRepository.saveAll(scenario.getBiasQuestions());
        return scenarioRepository.save(scenario.clone());
    }
}