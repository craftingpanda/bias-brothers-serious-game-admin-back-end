package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.bias.Bias;
import com.biasbrothers.seriousgame.biasQuestion.BiasQuestion;
import com.biasbrothers.seriousgame.biasQuestion.BiasQuestionController;
import com.biasbrothers.seriousgame.biasQuestion.BiasQuestionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = BiasQuestionController.class)
public class BiasQuestionTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private BiasQuestionService biasQuestionService; // mock the repository

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findBiasQuestionByIdTest() throws Exception {
        Bias bias = new Bias(
                1L,
                "Decoy effect",
                "In de keuze tussen twee producten zal er een voorkeur optreden voor een van de twee zodra je een er een optie tussen plaatst die (asymmetrisch) dichter bij een van de opties ligt.",
                "In de bioscoop verkoopt men kleine, medium en grote popcorn voor €2.50, €3,50 en €4,00 respectievelijk."
        );

        BiasQuestion biasQuestion = new BiasQuestion(
                1L,
                0,
                bias
                );
        Mockito.doReturn(biasQuestion).when(biasQuestionService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(biasQuestion);

        this.mvc.perform(get("/bias_question/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(biasQuestionService).getById(1L);
    }

    @Test
    public void addBiasQuestionTest() throws Exception {
        Bias bias = new Bias(
                "test",
                "test",
                "test"
        );

        BiasQuestion biasQuestion = new BiasQuestion(
                0,
                bias
        );

        Mockito.when(biasQuestionService.add(Mockito.any(BiasQuestion.class))).thenReturn(biasQuestion);

        final String expectedResponseContent = objectMapper.writeValueAsString(biasQuestion);

        this.mvc.perform(post("/bias_question/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteBiasQuestionTest() throws Exception {
        Mockito.when(biasQuestionService.delete(1L)).thenReturn("SUCCESS");
        mvc.perform(delete("/bias_question/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateBiasQuestionTest() throws Exception {
        Bias bias = new Bias("test", "test", "test");

        BiasQuestion biasQuestion = new BiasQuestion(
                0,
                bias
        );

        Mockito.when(biasQuestionService.update(Mockito.any(BiasQuestion.class))).thenReturn(biasQuestion);

        final String expectedResponseContent = objectMapper.writeValueAsString(biasQuestion);

        this.mvc.perform(put("/bias_question/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().json(expectedResponseContent));
    }

}
