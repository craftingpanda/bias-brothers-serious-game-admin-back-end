package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.bias.Bias;
import com.biasbrothers.seriousgame.biasQuestion.BiasQuestion;
import com.biasbrothers.seriousgame.canvas.Canvas;
import com.biasbrothers.seriousgame.canvas.metric.Metric;
import com.biasbrothers.seriousgame.measure.Measure;
import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestion;
import com.biasbrothers.seriousgame.newsarticle.NewsArticle;
import com.biasbrothers.seriousgame.round.Round;
import com.biasbrothers.seriousgame.round.RoundController;
import com.biasbrothers.seriousgame.round.RoundService;
import com.biasbrothers.seriousgame.scenario.Scenario;
import com.biasbrothers.seriousgame.timer.Timer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = RoundController.class)
public class RoundControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RoundService roundService;// mock the repository

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findRoundByIdTest() throws Exception {
        String title = "De start";
        int roundNumber = 1;
        List<Metric> metricList = new ArrayList<>();
        metricList.add(new Metric(1L, "Gezonde bevolking"));
        metricList.add(new Metric(2L, "Geinfecteerde bevolking"));
        metricList.add(new Metric(3L, "Gemuteerde bevolking"));

        List<NewsArticle> newsArticleList = new ArrayList<>();
        newsArticleList.add(new NewsArticle(
                1L,
                "Een nieuw en potentieel levensbedreigend virus is gevonden in Noord-Kropslavië",
                "De afgelopen dagen zijn er steeds meer gevallen bekend geworden van een nieuw en mogelijk levensbedreigend virus in Noord-Kropslavië. Genaamd de olifantengriep vanwege de gezichtsmutatie die het laatste stadium van besmetting met dit virus met zich mee brengt: een permanente verandering van de neus in een slurf. Epidemiologen en virologen zijn druk bezig met een onderzoek om het virus tegen te gaan.",
                "~ Daily Digitanzanië ~",
                true
        ));
        newsArticleList.add(new NewsArticle(
                2L,
                "Experts maken zich zorgen over stijgende aantal besmettingen Olifantengriep",
                "Veel blijft nog onbekend over de onlangs ontdekte Olifantengriep. Vooralsnog lijkt Noord-Kropslavië het enige land dat getroffen is. Inzichten in de verspreiding en werking van het virus blijven nog uit. Authoriteiten in Degressia zijn begonnen met de eerste maatregelen om het land voor te bereiden op een uitbraak.",
                "~ Degressia Dagblad ~",
                false
        ));

        Canvas canvas = new Canvas(
                1L,
                "G1R1Canvas3",
                3,
                "19362815",
                "0",
                "0",
                metricList,
                newsArticleList
        );

        Bias bias1 = new Bias(
                1L,
                "Decoy effect",
                "In de keuze tussen twee producten zal er een voorkeur optreden voor een van de twee zodra je een er een optie tussen plaatst die (asymmetrisch) dichter bij een van de opties ligt.",
                "In de bioscoop verkoopt men kleine, medium en grote popcorn voor €2.50, €3,50 en €4,00 respectievelijk."
        );
        Bias bias2 = new Bias(
                2L,
                "Representativeness Heuristic",
                "We hebben de neiging zaken die op elkaar lijken, die representatief zijn voor elkaar, met elkaar te verbinden. Ook als daar geen correlatie of causaal verband tussen bestaat.",
                "In sommige alternatieve geneeswijzen wordt het aangeraden om orgaanvlees te eten overeenkomstig met het orgaan waar de patiënt last van heeft. Het gezonde, geconsumeerde vlees zou dan helpen het zieke weefsel te helen."
        );
        Bias bias3 = new Bias(
                3L,
                "Negativity bias",
                "Negatieve prikkels hebben een disproportioneel groter effect op onze gemoedstoestand dan positieve prikkels van eenzelfde intensiteit.",
                "Je zit al een uur in de vergadering met de directie. Je realiseert je dat het 90% van de tijd gaat over risico's, verliezen, bedreigingen en het vermijden van deze zaken."
        );
        BiasQuestion biasQuestion1 = new BiasQuestion(
                1L,
                0,
                bias1
        );
        BiasQuestion biasQuestion2 = new BiasQuestion(
                2L,
                5,
                bias2
        );
        BiasQuestion biasQuestion3 = new BiasQuestion(
                3L,
                5,
                bias3
        );

        Measure measure1 = new Measure(
                1L,
                "We plaatsen een overzicht van de informatie over het virus en omschrijven welke soorten middelen infectie kunnen afremmen of tegenhouden."
        );

        Measure measure2 = new Measure(
                2L,
                "De bevolking van Digitanzanië is slim genoeg om te snappen dat dit soort huis, tuin en keukenmiddeltjes hen niet veilig gaan houden. We hoeven niks te doen."
        );

        Measure measure3 = new Measure(
                3L,
                "Een gevoel van veiligheid is ook wat waard, maar dat moet niet ten koste gaan van anderen. We laten weten dat de tampons niet zullen helpen."
        );

        MeasureQuestion measureQuestion1 = new MeasureQuestion(
                1L,
                5,
                measure1
        );
        MeasureQuestion measureQuestion2 = new MeasureQuestion(
                2L,
                0,
                measure2
        );
        MeasureQuestion measureQuestion3 = new MeasureQuestion(
                3L,
                0,
                measure3
        );

        List<Canvas> canvasList = new ArrayList<>();
        canvasList.add(canvas);
        List<BiasQuestion> biasQuestionList = new ArrayList<>();
        biasQuestionList.add(biasQuestion1);
        biasQuestionList.add(biasQuestion2);
        biasQuestionList.add(biasQuestion3);
        List<MeasureQuestion> measureQuestionList = new ArrayList<>();
        measureQuestionList.add(measureQuestion1);
        measureQuestionList.add(measureQuestion2);
        measureQuestionList.add(measureQuestion3);

        Scenario scenario = new Scenario(
                1L,
                "Hamsterwoede!",
                "Met oplopende Olifantengriepcijfers op de horizon zijn inwoners van Engelse Eiland massaal begonnen met het inslaan van tampons. Deze zouden helpen de Olifantengriep uit de neus te houden en daarmee een infectie te voorkomen. De regering van Engelse Eiland verzoekt haar burgers dringend om te stoppen met hamsteren, en verzekert hen ervan dat de voorraden groot genoeg zijn om iedereen te kunnen bedienen. Over de effectiviteit van het tampongebruik als preventie van de Olifantengriep zijn door experts nog geen uitspraken gedaan. In Digitanzanië beginnen de eerste geluiden al op te gaan om spullen in te slaan. Wat moet de overheid doen?",
                measureQuestionList,
                biasQuestionList
        );


        Timer timer = new Timer(
                3L,
                "7"
        );


        Round round = new Round(
                1L,
                title,
                roundNumber,
                canvasList,
                scenario,
                timer
        );

        Mockito.doReturn(round).when(roundService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(round);

        this.mvc.perform(get("/round/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(roundService).getById(1L);
    }

    @Test
    public void addRoundTest() throws Exception {
        String title = "De start";
        int roundNumber = 1;
        List<Metric> metricList = new ArrayList<>();
        metricList.add(new Metric(97L, "test"));
        metricList.add(new Metric(98L, "test"));
        metricList.add(new Metric(99L, "test"));

        List<NewsArticle> newsArticleList = new ArrayList<>();
        newsArticleList.add(new NewsArticle(
                98L,
                "test",
                "test",
                "test",
                true
        ));
        newsArticleList.add(new NewsArticle(
                99L,
                "test",
                "test",
                "test",
                false
        ));

        Canvas canvas = new Canvas(
                99L,
                "G1R1Canvas3",
                0,
                "19362815",
                "0",
                "0",
                metricList,
                newsArticleList
        );

        Bias bias1 = new Bias(
                97L,
                "test1",
                "test",
                "test."
        );
        Bias bias2 = new Bias(
                98L,
                "test2",
                "test",
                "test"
        );
        Bias bias3 = new Bias(
                99L,
                "test3",
                "test",
                "test"
        );
        BiasQuestion biasQuestion1 = new BiasQuestion(
                97L,
                0,
                bias1
        );
        BiasQuestion biasQuestion2 = new BiasQuestion(
                98L,
                5,
                bias2
        );
        BiasQuestion biasQuestion3 = new BiasQuestion(
                99L,
                5,
                bias3
        );

        Measure measure1 = new Measure(
                1L,
                "test1"
        );

        Measure measure2 = new Measure(
                2L,
                "test2"
        );

        Measure measure3 = new Measure(
                3L,
                "test3"
        );

        MeasureQuestion measureQuestion1 = new MeasureQuestion(
                1L,
                5,
                measure1
        );
        MeasureQuestion measureQuestion2 = new MeasureQuestion(
                2L,
                0,
                measure2
        );
        MeasureQuestion measureQuestion3 = new MeasureQuestion(
                3L,
                0,
                measure3
        );

        List<Canvas> canvasList = new ArrayList<>();
        canvasList.add(canvas);
        List<BiasQuestion> biasQuestionList = new ArrayList<>();
        biasQuestionList.add(biasQuestion1);
        biasQuestionList.add(biasQuestion2);
        biasQuestionList.add(biasQuestion3);
        List<MeasureQuestion> measureQuestionList = new ArrayList<>();
        measureQuestionList.add(measureQuestion1);
        measureQuestionList.add(measureQuestion2);
        measureQuestionList.add(measureQuestion3);

        Scenario scenario = new Scenario(
                99L,
                "test",
                "test",
                measureQuestionList,
                biasQuestionList
        );

        Timer timer = new Timer(
                99L,
                "7"
        );

        Round round = new Round(
                99L,
                title,
                roundNumber,
                canvasList,
                scenario,
                timer
        );


        Mockito.when(roundService.add(Mockito.any(Round.class))).thenReturn(round);

        final String expectedResponseContent = objectMapper.writeValueAsString(round);

        this.mvc.perform(post("/round/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteRoundTest() throws Exception {
        Mockito.when(roundService.delete(1L)).thenReturn("SUCCESS");
        mvc.perform(delete("/round/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateRoundTest() throws Exception {
        String title = "De start";
        int roundNumber = 1;
        List<Metric> metricList = new ArrayList<>();
        metricList.add(new Metric(1L, "test1"));
        metricList.add(new Metric(2L, "test2"));
        metricList.add(new Metric(3L, "test3"));

        List<NewsArticle> newsArticleList = new ArrayList<>();
        newsArticleList.add(new NewsArticle(
                1L,
                "test1",
                "test2",
                "test3",
                true
        ));
        newsArticleList.add(new NewsArticle(
                2L,
                "test1",
                "test2",
                "test3",
                false
        ));

        Canvas canvas = new Canvas(
                1L,
                "test1",
                0,
                "test4",
                "test5",
                "test6",
                metricList,
                newsArticleList
        );

        Bias bias1 = new Bias(
                1L,
                "test1",
                "test2",
                "test3."
        );
        Bias bias2 = new Bias(
                2L,
                "test4",
                "test5",
                "test6"
        );
        Bias bias3 = new Bias(
                3L,
                "test7",
                "test8",
                "test9"
        );
        BiasQuestion biasQuestion1 = new BiasQuestion(
                1L,
                0,
                bias1
        );
        BiasQuestion biasQuestion2 = new BiasQuestion(
                2L,
                5,
                bias2
        );
        BiasQuestion biasQuestion3 = new BiasQuestion(
                3L,
                5,
                bias3
        );

        Measure measure1 = new Measure(
                1L,
                "test1"
        );

        Measure measure2 = new Measure(
                2L,
                "test2"
        );

        Measure measure3 = new Measure(
                3L,
                "test3"
        );

        MeasureQuestion measureQuestion1 = new MeasureQuestion(
                1L,
                5,
                measure1
        );
        MeasureQuestion measureQuestion2 = new MeasureQuestion(
                2L,
                0,
                measure2
        );
        MeasureQuestion measureQuestion3 = new MeasureQuestion(
                3L,
                0,
                measure3
        );

        List<Canvas> canvasList = new ArrayList<>();
        canvasList.add(canvas);
        List<BiasQuestion> biasQuestionList = new ArrayList<>();
        biasQuestionList.add(biasQuestion1);
        biasQuestionList.add(biasQuestion2);
        biasQuestionList.add(biasQuestion3);
        List<MeasureQuestion> measureQuestionList = new ArrayList<>();
        measureQuestionList.add(measureQuestion1);
        measureQuestionList.add(measureQuestion2);
        measureQuestionList.add(measureQuestion3);

        Scenario scenario = new Scenario(
                1L,
                "test1",
                "test2",
                measureQuestionList,
                biasQuestionList
        );

        Timer timer = new Timer(
                1L,
                "7"
        );

        Round round = new Round(
                1L,
                title,
                roundNumber,
                canvasList,
                scenario,
                timer
        );

        Mockito.when(roundService.update(Mockito.any(Round.class))).thenReturn(round);

        String expectedResponseContent = objectMapper.writeValueAsString(round);

        this.mvc.perform(put("/round/update", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().json(expectedResponseContent));
    }
}
