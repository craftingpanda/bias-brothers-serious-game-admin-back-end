package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.gameLogic.GameLogicHandler;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CheckAnswerTests {

    private final GameLogicHandler gameLogicHandler = new GameLogicHandler();

    @Test
    public void canvasZeroPointsZero() {
        //given
        int canvasNumber = 0;
        int measurePoints = 0;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(0);
    }

    @Test
    public void canvasZeroPointsTwo() {
        //given
        int canvasNumber = 0;
        int measurePoints = 2;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(0);
    }

    @Test
    public void canvasZeroPointsFive() {
        //given
        int canvasNumber = 0;
        int measurePoints = 5;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(1);
    }

    @Test
    public void canvasOnePointsZero() {
        //given
        int canvasNumber = 1;
        int measurePoints = 0;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(0);
    }

    @Test
    public void canvasOnePointsTwo() {
        //given
        int canvasNumber = 1;
        int measurePoints = 2;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(1);
    }

    @Test
    public void canvasOnePointsFive() {
        //given
        int canvasNumber = 1;
        int measurePoints = 5;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(2);
    }

    @Test
    public void canvasTwoPointsZero() {
        //given
        int canvasNumber = 2;
        int measurePoints = 0;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(1);
    }

    @Test
    public void canvasTwoPointsTwo() {
        //given
        int canvasNumber = 2;
        int measurePoints = 2;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(2);
    }

    @Test
    public void canvasTwoPointsFive() {
        //given
        int canvasNumber = 2;
        int measurePoints = 5;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(3);
    }

    @Test
    public void canvasThreePointsZero() {
        //given
        int canvasNumber = 3;
        int measurePoints = 0;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(2);
    }

    @Test
    public void canvasThreePointsTwo() {
        //given
        int canvasNumber = 3;
        int measurePoints = 2;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(3);
    }

    @Test
    public void canvasThreePointsFive() {
        //given
        int canvasNumber = 3;
        int measurePoints = 5;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(4);
    }

    @Test
    public void canvasFourPointsZero() {
        //given
        int canvasNumber = 4;
        int measurePoints = 0;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(3);
    }

    @Test
    public void canvasFourPointsTwo() {
        //given
        int canvasNumber = 4;
        int measurePoints = 2;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(3);
    }

    @Test
    public void canvasFourPointsFive() {
        //given
        int canvasNumber = 4;
        int measurePoints = 5;
        //when
        int resultNewCanvasNumber = gameLogicHandler.checkAnswer(canvasNumber, measurePoints);
        //then
        assertThat(resultNewCanvasNumber).isEqualTo(4);
    }

}
