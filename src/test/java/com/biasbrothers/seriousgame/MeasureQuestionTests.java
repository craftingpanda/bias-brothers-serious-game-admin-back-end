package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.measure.Measure;
import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestion;
import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestionController;
import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = MeasureQuestionController.class)
public class MeasureQuestionTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MeasureQuestionService measureQuestionService; // mock the repository

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findMeasureQuestionByIdTest() throws Exception {

        MeasureQuestion measureQuestion = new MeasureQuestion(
                1L,
                5,
                new Measure("We plaatsen een overzicht van de informatie over het virus en omschrijven welke soorten middelen infectie kunnen afremmen of tegenhouden.")
        );

        Mockito.doReturn(measureQuestion).when(measureQuestionService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(measureQuestion);

        this.mvc.perform(get("/measure_question/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(measureQuestionService).getById(1L);
    }

    @Test
    public void addMeasureQuestionTest() throws Exception {
        MeasureQuestion measureQuestion = new MeasureQuestion(
                0,
                new Measure("")
        );

        Mockito.when(measureQuestionService.add(Mockito.any(MeasureQuestion.class))).thenReturn(measureQuestion);

        final String expectedResponseContent = objectMapper.writeValueAsString(measureQuestion);

        this.mvc.perform(post("/measure_question/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteMeasureQuestionTest() throws Exception {
        Mockito.when(measureQuestionService.delete(1L)).thenReturn("SUCCESS");
        mvc.perform(delete("/measure_question/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateMeasureQuestionTest() throws Exception {
        MeasureQuestion measureQuestion = new MeasureQuestion(
                0,
                new Measure("")
        );

        Mockito.when(measureQuestionService.update(Mockito.any(MeasureQuestion.class))).thenReturn(measureQuestion);

        final String expectedResponseContent = objectMapper.writeValueAsString(measureQuestion);

        this.mvc.perform(put("/measure_question/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().json(expectedResponseContent));
    }

}


