package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.canvas.Canvas;
import com.biasbrothers.seriousgame.canvas.CanvasController;
import com.biasbrothers.seriousgame.canvas.CanvasService;
import com.biasbrothers.seriousgame.canvas.metric.Metric;
import com.biasbrothers.seriousgame.newsarticle.NewsArticle;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CanvasController.class)
public class CanvasControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CanvasService canvasService;// mock the repository

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findCanvasByIdTest() throws Exception {
        List<Metric> metricList = new ArrayList<>();
        metricList.add(new Metric(1L, "Gezonde bevolking"));
        metricList.add(new Metric(2L, "Geinfecteerde bevolking"));
        metricList.add(new Metric(3L, "Gemuteerde bevolking"));

        List<NewsArticle> newsArticleList = new ArrayList<>();
        newsArticleList.add(new NewsArticle(
                1L,
                "Een nieuw en potentieel levensbedreigend virus is gevonden in Noord-Kropslavië",
                "De afgelopen dagen zijn er steeds meer gevallen bekend geworden van een nieuw en mogelijk levensbedreigend virus in Noord-Kropslavië. Genaamd de olifantengriep vanwege de gezichtsmutatie die het laatste stadium van besmetting met dit virus met zich mee brengt: een permanente verandering van de neus in een slurf. Epidemiologen en virologen zijn druk bezig met een onderzoek om het virus tegen te gaan.",
                "~ Daily Digitanzanië ~",
                true
        ));
        newsArticleList.add(new NewsArticle(
                2L,
                "Experts maken zich zorgen over stijgende aantal besmettingen Olifantengriep",
                "Veel blijft nog onbekend over de onlangs ontdekte Olifantengriep. Vooralsnog lijkt Noord-Kropslavië het enige land dat getroffen is. Inzichten in de verspreiding en werking van het virus blijven nog uit. Authoriteiten in Degressia zijn begonnen met de eerste maatregelen om het land voor te bereiden op een uitbraak.",
                "~ Degressia Dagblad ~",
                false
        ));

        Canvas canvas = new Canvas(
                "G1R1Canvas3",
                3,
                "19362815",
                "0",
                "0",
                metricList,
                newsArticleList
        );

        Mockito.doReturn(canvas).when(canvasService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(canvas);

        this.mvc.perform(get("/canvas/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(canvasService).getById(1L);
    }

    @Test
    public void addCanvasTest() throws Exception {

        List<Metric> metricList = new ArrayList<>();
        metricList.add(new Metric(1L, "Gezonde bevolking"));
        metricList.add(new Metric(2L, "Geinfecteerde bevolking"));
        metricList.add(new Metric(3L, "Gemuteerde bevolking"));

        List<NewsArticle> newsArticleList = new ArrayList<>();
        newsArticleList.add(new NewsArticle(
                1L,
                "Een nieuw en potentieel levensbedreigend virus is gevonden in Noord-Kropslavië",
                "De afgelopen dagen zijn er steeds meer gevallen bekend geworden van een nieuw en mogelijk levensbedreigend virus in Noord-Kropslavië. Genaamd de olifantengriep vanwege de gezichtsmutatie die het laatste stadium van besmetting met dit virus met zich mee brengt: een permanente verandering van de neus in een slurf. Epidemiologen en virologen zijn druk bezig met een onderzoek om het virus tegen te gaan.",
                "~ Daily Digitanzanië ~",
                true
        ));


        Canvas canvas = new Canvas(
                "test",
                0,
                "test",
                "test",
                "test",
                metricList,
                newsArticleList
        );

        Mockito.when(canvasService.add(Mockito.any(Canvas.class))).thenReturn(canvas);

        final String expectedResponseContent = objectMapper.writeValueAsString(canvas);

        this.mvc.perform(post("/canvas/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteCanvasTest() throws Exception {
        Mockito.when(canvasService.delete(1L)).thenReturn("SUCCES");
        mvc.perform(delete("/canvas/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateCanvasTest() throws Exception {
        List<Metric> metricList = new ArrayList<>();
        metricList.add(new Metric(1L, "test"));
        metricList.add(new Metric(2L, "test"));
        metricList.add(new Metric(3L, "test"));

        List<NewsArticle> newsArticleList = new ArrayList<>();
        newsArticleList.add(new NewsArticle(
                1L,
                "test",
                "test",
                "test",
                true
        ));
        newsArticleList.add(new NewsArticle(
                2L,
                "test",
                "test.",
                "~ test ~",
                false
        ));

        Canvas canvas = new Canvas(
                "test",
                0,
                "test",
                "test",
                "test",
                metricList,
                newsArticleList
        );

        Mockito.when(canvasService.update(Mockito.any(Canvas.class))).thenReturn(canvas);

        final String expectedResponseContent = objectMapper.writeValueAsString(canvas);

        this.mvc.perform(put("/canvas/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().json(expectedResponseContent));
    }
}
