package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.canvas.metric.Metric;
import com.biasbrothers.seriousgame.canvas.metric.MetricController;
import com.biasbrothers.seriousgame.canvas.metric.MetricService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = MetricController.class)
public class MetricControllerTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MetricService metricService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findMetricByIdTest() throws Exception {
        Metric metric = new Metric("Gezonde bevolking");

        Mockito.doReturn(metric).when(metricService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(metric);

        this.mvc.perform(get("/metric/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(metricService).getById(1L);
    }

    @Test
    public void addMetricTest() throws Exception {
        Metric metric = new Metric("add");

        Mockito.when(metricService.add(Mockito.any(Metric.class))).thenReturn(metric);

        final String expectedResponseContent = objectMapper.writeValueAsString(metric);

        this.mvc.perform(post("/metric/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteMetricTest() throws Exception {
        Mockito.when(metricService.delete(1L)).thenReturn("SUCCES");
        mvc.perform(delete("/metric/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateMetricTest() throws Exception {
        Metric metric = new Metric("update");

        Mockito.when(metricService.update(Mockito.any(Metric.class))).thenReturn(metric);

        final String expectedResponseContent = objectMapper.writeValueAsString(metric);

        this.mvc.perform(put("/metric/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                        .andExpect(status().isAccepted())
                        .andExpect(content().json(expectedResponseContent));
    }
}
