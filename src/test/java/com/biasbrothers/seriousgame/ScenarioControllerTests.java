package com.biasbrothers.seriousgame;

import com.biasbrothers.seriousgame.bias.Bias;
import com.biasbrothers.seriousgame.biasQuestion.BiasQuestion;
import com.biasbrothers.seriousgame.measure.Measure;
import com.biasbrothers.seriousgame.measureQuestion.MeasureQuestion;
import com.biasbrothers.seriousgame.scenario.Scenario;
import com.biasbrothers.seriousgame.scenario.ScenarioController;
import com.biasbrothers.seriousgame.scenario.ScenarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ScenarioController.class)
public class ScenarioControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ScenarioService scenarioService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void findScenarioByIdTest() throws Exception {
        List<MeasureQuestion> measureQuestions = new ArrayList<>();
        measureQuestions.add(new MeasureQuestion(5 , new Measure("We plaatsen een overzicht van de informatie over het virus en omschrijven welke soorten middelen infectie kunnen afremmen of tegenhouden.")));
        measureQuestions.add(new MeasureQuestion(0 , new Measure("De bevolking van Digitanzanië is slim genoeg om te snappen dat dit soort huis, tuin en keukenmiddeltjes hen niet veilig gaan houden. We hoeven niks te doen.")));
        measureQuestions.add(new MeasureQuestion(2 , new Measure("Een gevoel van veiligheid is ook wat waard, maar dat moet niet ten koste gaan van anderen. We laten weten dat de tampons niet zullen helpen.")));
        List<BiasQuestion> biasQuestions = new ArrayList<>();
        biasQuestions.add(new BiasQuestion(5 , new Bias("Decoy effect","In de keuze tussen twee producten zal er een voorkeur optreden voor een van de twee zodra je een er een optie tussen plaatst die (asymmetrisch) dichter bij een van de opties ligt.","In de bioscoop verkoopt men kleine, medium en grote popcorn voor €2.50, €3,50 en €4,00 respectievelijk.")));
        biasQuestions.add(new BiasQuestion(0 , new Bias("Representativeness Heuristic","We hebben de neiging zaken die op elkaar lijken, die representatief zijn voor elkaar, met elkaar te verbinden. Ook als daar geen correlatie of causaal verband tussen bestaat.","In sommige alternatieve geneeswijzen wordt het aangeraden om orgaanvlees te eten overeenkomstig met het orgaan waar de patiënt last van heeft. Het gezonde, geconsumeerde vlees zou dan helpen het zieke weefsel te helen.")));
        biasQuestions.add(new BiasQuestion(2 , new Bias("Negativity bias","Negatieve prikkels hebben een disproportioneel groter effect op onze gemoedstoestand dan positieve prikkels van eenzelfde intensiteit.","Je zit al een uur in de vergadering met de directie. Je realiseert je dat het 90% van de tijd gaat over risico's, verliezen, bedreigingen en het vermijden van deze zaken.")));

        Scenario scenario = new Scenario(
                "Hamsterwoede!",
                "Met oplopende Olifantengriepcijfers op de horizon zijn inwoners van Engelse Eiland massaal begonnen met het inslaan van tampons. Deze zouden helpen de Olifantengriep uit de neus te houden en daarmee een infectie te voorkomen. De regering van Engelse Eiland verzoekt haar burgers dringend om te stoppen met hamsteren, en verzekert hen ervan dat de voorraden groot genoeg zijn om iedereen te kunnen bedienen. Over de effectiviteit van het tampongebruik als preventie van de Olifantengriep zijn door experts nog geen uitspraken gedaan. In Digitanzanië beginnen de eerste geluiden al op te gaan om spullen in te slaan. Wat moet de overheid doen?", measureQuestions, biasQuestions);
                Mockito.doReturn(scenario).when(scenarioService).getById(1L);

        final String expectedResponseContent = objectMapper.writeValueAsString(scenario);

        this.mvc.perform(get("/scenario/?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponseContent));

        verify(scenarioService).getById(1L);
    }

    @Test
    public void addNewScenarioTest() throws Exception {
        MeasureQuestion question1 = new MeasureQuestion(0 , new Measure(""));
        MeasureQuestion question2 = new MeasureQuestion(0 , new Measure(""));
        MeasureQuestion question3 = new MeasureQuestion(0 , new Measure(""));
        List<MeasureQuestion> measureQuestions = new ArrayList<>();
        measureQuestions.add(question1);
        measureQuestions.add(question2);
        measureQuestions.add(question3);
        List<BiasQuestion> biasQuestions = new ArrayList<>();
        biasQuestions.add(new BiasQuestion(0 , new Bias("", "", "")));
        biasQuestions.add(new BiasQuestion(0 , new Bias("", "", "")));
        biasQuestions.add(new BiasQuestion(0 , new Bias("", "", "")));

        Scenario scenario = new Scenario("add", "add", measureQuestions,biasQuestions);

        Mockito.when(scenarioService.add(Mockito.any(Scenario.class))).thenReturn(scenario);

        final String expectedResponseContent = objectMapper.writeValueAsString(scenario);

        this.mvc.perform(post("/scenario/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(expectedResponseContent));
    }

    @Test
    public void deleteScenarioTest() throws Exception {
        Mockito.when(scenarioService.delete(1L)).thenReturn("SUCCESS");
        mvc.perform(delete("/scenario/delete/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    public void updateScenarioTest() throws Exception {
        MeasureQuestion question1 = new MeasureQuestion(0 , new Measure(""));
        MeasureQuestion question2 = new MeasureQuestion(0 , new Measure(""));
        MeasureQuestion question3 = new MeasureQuestion(0 , new Measure(""));
        BiasQuestion biasQuestion1 = new BiasQuestion(0 , new Bias("", "", ""));
        BiasQuestion biasQuestion2 = new BiasQuestion(0 , new Bias("", "", ""));
        BiasQuestion biasQuestion3 = new BiasQuestion(0 , new Bias("", "", ""));
        List<MeasureQuestion> measureQuestions = new ArrayList<>();
        List<BiasQuestion> biasQuestions = new ArrayList<>();
        biasQuestions.add(biasQuestion1);
        biasQuestions.add(biasQuestion2);
        biasQuestions.add(biasQuestion3);
        measureQuestions.add(question1);
        measureQuestions.add(question2);
        measureQuestions.add(question3);
        Scenario scenario = new Scenario("update", "update", measureQuestions, biasQuestions);
        Mockito.doReturn(scenario).when(scenarioService).add(scenario);

        Mockito.when(scenarioService.update(Mockito.any(Scenario.class))).thenReturn(scenario);

        final String expectedResponseContent = objectMapper.writeValueAsString(scenario);

        this.mvc.perform(put("/scenario/update/", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8")
                        .content(expectedResponseContent)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(content().json(expectedResponseContent));
    }

}
