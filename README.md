# Bias Brothers - Serious Game Admin back-end
This project is made for the Bias Brothers so they could
edit the content of the previous project we made as a group of students.
We made this project in 4 sprints of 2 weeks.

# Information
Load maven project, SDK: corretto-11 java version "11.0.13"   -    Language level 11 <br> 
This project makes use of a database in PostgreSQL and uses Spring boot.
see application.properties file for port, & postgres login.

# All other projects listed
Underneath you can find the name and location of the projects, see its own README to know which branches work together.
-----------------------------------------------------------------------------------------------------------------------
The front-end of the serious game application: Bias Brothers - Serious Game <br>
You can find at: https://gitlab.com/craftingpanda/bias-brothers-serious-game

The back-end in javaSpark for the serious game: Bias Brothers - Serious Game back-end <br>
You can find at: https://gitlab.com/craftingpanda/bias-brothers-serious-game-back-end

The front-end in React of the serious game admin: Bias Brothers - Serious Game Admin <br>
You can find at: https://gitlab.com/craftingpanda/bias-brothers-serious-game-admin

The new back-end of the serious game application & admin side : Bias Brothers - Serious Game Admin back-end <br>
You can find at: https://gitlab.com/craftingpanda/bias-brothers-serious-game-admin-back-end

# Version 1 - Team project
This version works with branch version 1 of the front-end in React of this project see link above for the repository.

# Version 2 - Finishing all 4 projects on my own
This project was to make it so the old back-end was not needed anymore.

This back-end provides for the serious game & serious game admin version 2 see link above for the repository.

# Full list of all projects and branches that work together
# Bias Brother - Serious Game
Serious Game Version 1 - Serious Game back-end Version 1 <br>
Serious Game Version 2 - Serious Game back-end Version 2 <br>
Serious Game Version 3 - Serious Game Admin Version 2

# Bias Brother - Serious Game back-end
Serious Game back-end Version 1 - Serious Game Version 1 <br>
Serious Game back-end Version 2 - Serious Game Version 2

# Bias Brother - Serious Game Admin
Serious Game Admin Version 1 - Serious Game Admin back-end Version 1 <br>
Serious Game Admin Version 2 - Serious Game Version 3 <br>
Serious Game Admin Version 2 - Serious Game Admin back-end Version 2

# Bias Brother - Serious Game Admin back-end
Serious Game Admin back-end Version 1 - Serious Game Admin Version 1 <br>
Serious Game Admin back-end Version 2 - Serious Game Version 3 <br>
Serious Game Admin back-end Version 2 - Serious Game Admin Version 2